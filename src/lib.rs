use std::collections::HashMap;
use std::sync::RwLock;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::{Element, HtmlObjectElement, KeyboardEvent, Window, XmlDocument};

const KEY_DOWN_COLOR: &str = "green";

struct CacheEntry {
    original_fill: String,
    background: Element,
}

fn bind_key_handler(
    type_: &str,
    window: &Window,
    keyboard_svg: &XmlDocument,
    callback: impl FnMut(KeyboardEvent) + 'static,
) {
    let closure = Closure::wrap(Box::new(callback) as Box<dyn FnMut(_)>);
    let js_function = closure.as_ref().unchecked_ref();
    window
        .add_event_listener_with_callback(type_, js_function)
        .expect(&format!(
            "couldn't add event listener '{}' to window",
            type_
        ));
    // The svg is technically an other document loaded like an iframe.
    // It catches its own events which don't bubble up to the root window :(
    keyboard_svg
        .add_event_listener_with_callback(type_, js_function)
        .expect(&format!("couldn't add event listener '{}' to svg", type_));
    closure.forget();
}

fn fill(background: &Element, color: &str) {
    background
        .set_attribute("fill", color)
        .expect("Couldn't set 'fill' attribute.");
}

fn highlight(background: &Element) {
    fill(background, KEY_DOWN_COLOR);
}

#[wasm_bindgen(start)]
pub fn run() {
    // Initialize debugging for when/if something goes wrong.
    console_error_panic_hook::set_once();

    let window = web_sys::window().unwrap();
    let keyboard: &_ = Box::leak(Box::new(
        window
            .document()
            .unwrap()
            .get_element_by_id("keyboard")
            .unwrap()
            .unchecked_into::<HtmlObjectElement>()
            .content_document()
            .unwrap()
            .unchecked_into::<XmlDocument>(),
    ));

    let cache: &_ = Box::leak(Box::new(RwLock::new(HashMap::new())));

    bind_key_handler("keydown", &window, keyboard, move |kbevent| {
        if kbevent.repeat() {
            return;
        }
        let code = kbevent.code();
        if let Some(CacheEntry { background, .. }) = cache.read().unwrap().get(&code) {
            highlight(background);
            return;
        }
        if let Some(txt_node) = keyboard.get_element_by_id(&format!("{}-txt", &code)) {
            let mut key = kbevent.key();
            if key == "Dead" {
                key = "�".to_string();
            } else {
                key.make_ascii_uppercase();
            }
            txt_node.set_inner_html(&key);
        }
        if let Some(background) = keyboard.get_element_by_id(&format!("{}-bg", code)) {
            let original_fill = background
                .get_attribute("fill")
                .expect("No 'fill' attribute on element!");
            highlight(&background);
            cache.write().unwrap().insert(
                code,
                CacheEntry {
                    original_fill,
                    background,
                },
            );
        }
    });
    bind_key_handler("keyup", &window, keyboard, move |kbevent| {
        let code = kbevent.code();
        let cache = cache.read().unwrap();
        if let Some(CacheEntry {
            background,
            original_fill,
        }) = cache.get(&code)
        {
            fill(background, original_fill);
        }
    });
}
