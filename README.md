# Web Keyboard

## Source

See `src/lib.rs`.

## Build

```
wasm-pack build --target web --out-dir static/
```

## Serve

Use any HTTP server, for example:

```
python -m http.server --directory static/
```

## Licence

[`static/keyboard.svg`](static/keyboard.svg) is CC-BY-SA 3.0 by Mysid, Ymulleneers, Azaghal of Belegost, Euka, Thibault Lemaire, Oscar Hemelaar. See [wikicommons page].

[wikicommons page]: https://commons.wikimedia.org/wiki/File:Qwerty.svg
