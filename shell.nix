{ pkgs ? import <nixpkgs> {} }:

with pkgs;
mkShell {
  buildInputs = [
    rustup
    wasm-pack
    python3Packages.python
  ];
}
